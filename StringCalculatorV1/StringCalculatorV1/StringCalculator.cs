﻿using System;
using System.Collections.Generic;
namespace StringCalculatorV1
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {

            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }
            List<string> DelimiterList = GetDelimiters(numbers);
            string[] numberList = numbers.Split(DelimiterList.ToArray(), StringSplitOptions.RemoveEmptyEntries);
           
            NegativeNumbersThrowException(numberList);
            if (numbers.Contains("/")) 
            {
                InvalidDelimiterThrowException(numberList);

            }
            int total = GetTotal(numberList); 
            return total ;
        }

        private int GetTotal(string[] numberList) 
        {
            int sum=0;
            foreach(var number in numberList)
            {
                if (!string.IsNullOrEmpty(number)) 
                {
                    if (int.Parse(number) <= 1000) 
                    {
                        sum += int.Parse(number);
                    }
                }
            }
            return sum; 
        }

        private List<string> GetDelimiters(string numbers) 
        {
            List<string> DelimiterList = new List<string>(new string[] { ",","\n"});
            if (numbers.Contains("/"))
            {
                DelimiterList = new List<string>(new string[] { "\n" }); 
                var Delimiters = numbers.Substring(0, numbers.IndexOf("\n"));
                foreach(var Delimiter in Delimiters) 
                {
                    DelimiterList.Add(Delimiter.ToString());
                }
            }
            return DelimiterList;
        }

        private void NegativeNumbersThrowException(string[] numberList) 
        {
            int Number; 
            string NegativeNumbers = string.Empty; 
            foreach (var item in numberList) 
            {
                if (int.TryParse(item, out Number) && !string.IsNullOrEmpty(item))
                {
                    if (Number < 0)
                    {
                        NegativeNumbers = NegativeNumbers + item; 
                    }
                }
            }

            if (NegativeNumbers != string.Empty)
            {
                throw new ArgumentException("Negatives not allowed." + NegativeNumbers);
            }
        }

        private void InvalidDelimiterThrowException(string[] numberList) 
        {            
            string InvalidDelimters = null; 
            foreach (var stringNumber  in numberList) 
            {
                foreach (var character in stringNumber)
                {
                    int Number; 
                    if (!int.TryParse(character.ToString(), out Number))
                    {
                        InvalidDelimters += character; 

                    }
                }
            }

            if (InvalidDelimters != string.Empty) 
            {
                //throw new ArgumentException("Invalid Delimiter !." + InvalidDelimters);
            }
        }
    }
}
